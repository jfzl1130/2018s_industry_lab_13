package ictgradschool.industry.concurrency.ex04;

import java.util.List;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;

public class ConcurrentBankingApp {

    public static void main(String[] args) {

        final BlockingQueue<Transaction> queue = new
                ArrayBlockingQueue<>(10);


        BankAccount account = new BankAccount();

        Thread producer = new Thread(new Runnable() {
            @Override
            public void run() {
                List<Transaction> transactions = TransactionGenerator.readDataFile();
                for(Transaction trans : transactions) {
                    try {
                        queue.put(trans);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }

            }
        });

        Thread consumer1 = new Thread(new Consumer(queue, account));

        Thread consumer2 = new Thread(new Consumer(queue, account));


        producer.start();
        consumer1.start();
        consumer2.start();


            try {
                producer.join();
                consumer1.interrupt();
                consumer2.interrupt();


            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            System.out.println("Final balance: " + account.getFormattedBalance());

    }
}
