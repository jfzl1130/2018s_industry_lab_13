package ictgradschool.industry.concurrency.ex05;

public enum TaskInfo {

    Initialized, Aborted, Completed
}
