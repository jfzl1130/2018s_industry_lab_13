package ictgradschool.industry.concurrency.ex05;

import ictgradschool.Keyboard;

import java.util.List;

public class Welcome {

    static boolean cancelled = false;

    public static void main (String[] args){
        System.out.println("Please enter your prime number!");
        Long userInput = Long.parseLong(Keyboard.readInput());
        PrimeFactorsTask p = new PrimeFactorsTask(userInput);
        Thread thread = new Thread(p);
        Thread thread1 = new Thread(new Runnable() {
            @Override
            public void run() {
                String userInput1 = Keyboard.readInput();
                thread.interrupt();
                System.out.print("The Thread has been Terminated");
                cancelled = true;


            }});
        thread.start();
        thread1.start();
        try {
            thread.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        List<Long> primeList = p.getPrimeList();
        if (!cancelled){
            for (Long prime: primeList
            ) {
                System.out.print(prime+"\n");
            }}
    }
}




